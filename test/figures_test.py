# добавить pytest.ini с марками для смоук и регресса и вызвать их в тесте
# добавить в pytest.ini addopts
# добавить файл conftest.py и сделать 5 фикстур (3 которые зависят друг от друга), и использовать их в тесте
# решить задачку с leedcode c суммами
import pytest
from HW1_duble import Circle, Rectangle, Square


@pytest.mark.parametrize(("expected_firstname", "expected_lastname", "expected_patronymic"),
                         [pytest.param("Nikita", "Sergeev", "Ivanovich", marks=pytest.mark.smoke, id="Check FIO positive test"),])
def test_check_parsed_fio(check_fio, expected_firstname, expected_lastname, expected_patronymic):

    check_lastname = check_fio.get("lastname", f"There is no key {check_fio}")
    check_firstname = check_fio.get("firstname", f"There is no key {check_fio}")
    check_patronymic = check_fio.get("patronymic", f"There is no key {check_fio}")

    assert check_lastname == expected_lastname, f"Actual lastname {check_lastname} don't equal expected lastname {expected_lastname}"
    assert check_firstname == expected_firstname, f"Actual firstname {check_firstname} don't equal expected firstname {expected_firstname}"
    assert check_patronymic == expected_patronymic, f"Actual patronymic {check_patronymic} don't equal expected patronymic {expected_patronymic}"


@pytest.mark.parametrize(("side_a", "side_b", "expected_result"),
                         [pytest.param(1, 2, 2, marks=pytest.mark.smoke, id="Rectangle(positive): int value"),
                          pytest.param(0.000000001, 2, 2e-09, marks=pytest.mark.smoke,
                                       id="Rectangle(positive): float value"),
                          pytest.param(5, 2, 10, marks=pytest.mark.regress, id="Rectangle(positive): float value")])
def test_rectangle_positive(decoration_after_test, side_a, side_b, expected_result):
    rectangle = Rectangle(side_a, side_b, "Rectangle")
    print()

    assert rectangle.get_area() == expected_result, f"Actual result{rectangle.get_area()} don't equal expected result {expected_result} for rectangle"


@pytest.mark.parametrize(("radius", "expected_result"),
                         [pytest.param(1, 3.14, marks=pytest.mark.smoke, id="Circle(positive): int value"),
                          pytest.param(0.000000001, 3.1400000000000002e-18, marks=pytest.mark.smoke,
                                       id="Circle(positive): float value"),
                          pytest.param(1, 3.14, marks=pytest.mark.regress, id="Circle(positive): int value")])
def test_circle_positive(decoration_after_test, radius, expected_result):
    circle = Circle(radius, "Circle")

    assert circle.get_area() == expected_result, f"Actual result{circle.get_area()} don't equal expected result {expected_result} for circle"


@pytest.mark.parametrize(("side", "expected_result"),
                         [pytest.param(1, 1, marks=pytest.mark.smoke, id="Square(positive): int value"),
                          pytest.param(1000.001, 1000002.000001, marks=pytest.mark.smoke,
                                       id="Square(positive): float value"),
                          pytest.param(2, 4, marks=pytest.mark.regress, id="Square(positive): int value")])
def test_square_positive(side, expected_result):
    square = Square(side, "Square")

    assert square.get_area() == expected_result, f"Actual result{square.get_area()} don't equal expected result {expected_result} for square"


@pytest.mark.parametrize(("side_a", "side_b", "expected_result"),
                         [pytest.param(0, 1, ValueError, marks=pytest.mark.smoke, id="Rectangle(negative): zero value"),
                          pytest.param("0", 1, TypeError, marks=pytest.mark.smoke,
                                       id="Rectangle(negative): string value"),
                          pytest.param(None, 1, TypeError, marks=pytest.mark.regress,
                                       id="Rectangle(negative): nonetype value")])
def test_rectangle_negative(decoration_after_test, side_a, side_b, expected_result):
    with pytest.raises(expected_result):
        rectangle = Rectangle(side_a, side_b, "Rectangle")
        rectangle.get_area()


@pytest.mark.parametrize(("radius", "expected_result"),
                         [pytest.param(0, ValueError, marks=pytest.mark.smoke, id="Circle(negative): zero value"),
                          pytest.param("0", TypeError, marks=pytest.mark.smoke, id="Circle(negative): string value"),
                          pytest.param(None, TypeError, marks=pytest.mark.regress,
                                       id="Circle(negative): nontype value")])
def test_circle_negative(radius, expected_result):
    with pytest.raises(expected_result):
        cicrle = Circle(radius, "Circle")
        cicrle.get_area()


@pytest.mark.parametrize(("side", "expected_result"),
                         [pytest.param(0, ValueError, marks=pytest.mark.smoke, id="Square(negative): zero value"),
                          pytest.param("0", TypeError, marks=pytest.mark.smoke, id="Square(negative): string value"),
                          pytest.param(None, TypeError, marks=pytest.mark.regress,
                                       id="Square(negress): nonetype value")])
def test_square_negative(side, expected_result):
    with pytest.raises(expected_result):
        square = Square(side, "Square")
        square.get_area()
