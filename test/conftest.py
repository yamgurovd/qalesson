import pytest


@pytest.fixture(scope="function")
def decoration_test():
    print("\nStart test")

    yield
    print("\nEnd test")


@pytest.fixture(scope="function")
def decoration_base_test(decoration_test):
    print("\nStart execute base test")

    yield
    print("\nEnd execute base test")
@pytest.fixture(scope="function")
def decoration_after_test(decoration_base_test):
    print("\nStart execute base after test")

    yield
    print("\nEnd execute base after test")

@pytest.fixture(scope="function")
def check_fio():
    print("\nStart checking FIO person !!!")
    fio = {
        "firstname": "Nikita",
        "lastname": "Sergeev",
        "patronymic": "Ivanovich"
    }

    yield fio
    print("\nEnd checking FIO person !!!")
