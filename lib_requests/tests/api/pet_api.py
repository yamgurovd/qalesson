import requests
from requests import Request

api_client = requests.session()
api_client.headers({"Content-Type": "aplication/json"})

base_url = "https://petstore.swagger.io/v2"


def create_pet(pet_id: int, category_id: int,  pet_name: str, category_name: str = "dog",
               status: str = "available") -> dict:
    json = {
        "id": pet_id,
        "category": {
            "id": category_id,
            "name": category_name
        },
        "name": pet_name,
        "status": status
    }

    pure_request = Request(
        method="POST",
        url=f"{base_url}/pet",
        json=json
    )
    request = api_client.prepare_request(request=pure_request)
    response = api_client.send(request=request)
