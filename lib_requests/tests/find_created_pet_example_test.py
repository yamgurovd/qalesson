import random

import requests
from requests import Request

api_client = requests.session()
api_client.headers.update({"Content-Type": "application/json"})

base_url = "https://petstore.swagger.io/v2"


def test_firnd_created_pet():
    id_ = random.randint(100, 1000)
    name = f"Murka{random.randint(10000, 500000)}"
    json_ = {
        "id": id_,
        "category": {
            "id": random.randint(5, 20),
            "name": "dog"
        },
        "name": name,
        "status": "available"
    }

    pure_request_post = Request(
        method="Post",
        url=f"{base_url}/pet",
        json=json_
    )

    request = api_client.prepare_request(request=pure_request_post)
    responce = api_client.send(request=request)

    print(f"\n{responce.json()}")

    assert responce.json().get("id", "test_default") == id_, f"Actual result {responce.json().get('id')}, not equal expected result {id_} in method POST"
    assert responce.json().get("name", "test_default") == name, f"Actual result {responce.json().get('name')}, not equal expected result {name} in method POST"

    pure_request_get = Request(
        method="GET",
        url=f"{base_url}/pet/{id_}",
    )
    request = api_client.prepare_request(request=pure_request_get)
    responce = api_client.send(request=request)
    print(f"\n{responce.json()}")

    assert responce.json().get("id", "test_default"), f"Actual result {responce.json().get('id')}, not equal expected result {id_} in method GET"
    assert responce.json().get("name", "test_default"), f"Actual result {responce.json().get('name')}, not equal expected result {name} in method GET"
