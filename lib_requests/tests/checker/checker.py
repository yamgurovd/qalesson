import requests
from requests import Request, Response


def check_status_code(response: Response, status_code: list[int], check_error: bool = False) -> dict:
    assert response.status_code in status_code, f"Actual status {response.status_code} not in list {status_code}"